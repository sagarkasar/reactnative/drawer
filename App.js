import { StatusBar } from 'expo-status-bar';
import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Routes from './src/Navigation/Routes';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import NavigationStrings from './src/Constants/NavigationStrings';
import TabRoutes from './src/Navigation/TabRoutes';
import DrawerNavigation from './src/Navigation/DrawerNavigation';

const  Stack= createNativeStackNavigator();


function App() {
  return (
    
    <NavigationContainer>
       <Stack.Navigator screenOptions={{headerShown:false}}>
        <Stack.Screen 
        name={NavigationStrings.TAB}
        component={TabRoutes}
        />
       
       </Stack.Navigator>
    </NavigationContainer>
    
  );
}

export default App;
