
export default {
    HOME: "Home",
    PROFILE: "Profile",
    ACCOUNT: "Account",
    TAB: 'Tabs',
    PRODUCTDETAILS: 'ProductDetails',
    EDITPROFILE: 'EditProfile',
    EDITACCOUNT: 'EditAccount',
    DRAWER: 'Drawer'
}