import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const EditAccount = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Edit Account Page</Text>
        </View>
    )
}

export default EditAccount

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
      color:'red',
      fontSize:25
  }
})
