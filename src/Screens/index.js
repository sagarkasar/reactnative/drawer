export { default as Home } from './Home';
export { default as Account } from './Account';
export { default as Profile } from './Profile';
export { default as ProductDetails } from './ProductDetails';
export { default as EditAccount } from './EditAccount';
export { default as EditProfile } from './EditProfile';
