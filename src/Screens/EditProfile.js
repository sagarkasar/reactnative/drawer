import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const EditProfile = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Edit Profile Page</Text>
        </View>
    )
}

export default EditProfile

const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text:{
      color:'red',
      fontSize:25
  }
})
