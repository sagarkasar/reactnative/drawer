import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Account, Home, Profile } from '../Screens';
import NavigationStrings from '../Constants/NavigationStrings';
import TabRoutes from './TabRoutes';

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
    return (
    <Drawer.Navigator initialRouteName={NavigationStrings.TAB} >
      <Drawer.Screen name={NavigationStrings.TAB} component={TabRoutes} />
      <Drawer.Screen name={NavigationStrings.HOME} component={Home} />
      <Drawer.Screen name={NavigationStrings.ACCOUNT} component={Account} />
    </Drawer.Navigator>
    )
}

export default DrawerNavigation;

const styles = StyleSheet.create({

})
