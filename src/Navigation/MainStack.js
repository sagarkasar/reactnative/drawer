import React from "react";
import NavigationStrings from "../Constants/NavigationStrings";
import TabRoutes from "./TabRoutes";


export default function (Stack){
    return(

        <>
        <Stack.Screen 
        name={NavigationStrings.TAB}
        component={TabRoutes}
        />
        </>
    )
}